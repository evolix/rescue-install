#!/bin/bash

# Ce script est une adaptation par jlecour du script de bserie
# Voici les adaptations :
# - /boot fait 500M
# - pas de LVM
# - on n'installe que le paquet virtuel linux-image-amd64 (pas le paquet réel)

echo "ce script ne doit pas être joué automatiquement"
echo "les commandes sont à jouer manuellement, par copier/coller"
exit 1

# Le script suppose que vous avez installé votre dedibox avec le partionnement par défaut.
# /boot sda1 2OOM, / sda2 2G, swap 1G sda3.
# / doit être sur sda2. Sinon adapter le script !
# Enfin un fstab est généré, grub et le kernel sont réinstallés.

ROOT_PART=/dev/sda2

export LC_ALL=C
# Dirty hack for rescue Ubuntu 16.04
touch /boot/vmlinuz-pouet
touch /boot/initrd.img-pouet

# disable swap
swapoff -a

cd /mnt

# mount /
mkdir root_in_ram rootfs home var usr log
mount $ROOT_PART /mnt/rootfs/

# copy all the filesystem in ram
mount -t tmpfs none /mnt/root_in_ram -o size=90%
rsync -a /mnt/rootfs/ /mnt/root_in_ram/

# unount rootfs to recreate partitions
umount /mnt/rootfs

# NOTE: sleep 1 second between each command
#       to be able to copy/paste the whole bloc

# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt
sleep 1
# bios_grub GPT *mandatory*
parted -a minimal -s /dev/sda mkpart primary 0M 1M
sleep 1
parted -s /dev/sda set 1 bios_grub on
sleep 1
# /boot
parted -a minimal -s /dev/sda mkpart primary ext4 1M 500M
sleep 1
parted -s /dev/sda set 2 boot on
sleep 1
mkfs.ext4 -LBOOT /dev/sda2 1>/dev/null
sleep 1
# /
parted -a minimal -s /dev/sda mkpart primary ext4 500M 1500M
sleep 1
mkfs.ext4 -LROOTFS /dev/sda3 1>/dev/null
sleep 1
# /var
parted -a minimal -s /dev/sda mkpart primary ext4 1500M 11500M
sleep 1
mkfs.ext4 -LVAR /dev/sda4 1>/dev/null
sleep 1
# /usr
parted -a minimal -s /dev/sda mkpart primary ext4 11500M 16500M
sleep 1
mkfs.ext4 -LUSR /dev/sda5 1>/dev/null
sleep 1
# /tmp
parted -a minimal -s /dev/sda mkpart primary ext4 16500M 17500M
sleep 1
mkfs.ext4 -LTMP /dev/sda6 1>/dev/null
sleep 1
# swap1
parted -a minimal -s /dev/sda mkpart primary linux-swap 17500M 18000M
sleep 1
mkswap -f -LSWAP1 /dev/sda7 1>/dev/null
sleep 1
# swap2
parted -a minimal -s /dev/sda mkpart primary linux-swap 18000M 18500M
sleep 1
mkswap -f -LSWAP2 /dev/sda8 1>/dev/null
sleep 1
# /home
parted -a minimal -s /dev/sda mkpart primary ext4 18500M 100%
sleep 1
mkfs.ext4 -LHOME /dev/sda9 1>/dev/null
sleep 1

# mount partitions
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mount -LUSR usr

# Copy data from RAM.
rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/

umount home var usr

# Generate fstab.
cat <<EOT > rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
EOT

# Chroot + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*
mount -LUSR /mnt/rootfs/usr
chroot /mnt/rootfs/ bash

export LC_ALL=C
mount /boot
mount /var
mount /tmp
chmod 1777 /tmp

apt purge -y grub-common grub-pc grub-pc-bin grub2-common
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
apt install -y grub-common grub-pc grub-pc-bin grub2-common linux-image-amd64
apt install -y sudo python python-apt

# Installer Grub et recharger sa configuration
grub-install /dev/sda
update-grub

# ajouter l'utilisateur dans le groupe sudo
# ça facilite le passage d'Ansible (sans root)
usermod -a -G sudo jlecour

# change root password
passwd
# permit root login via SSH with password
sed -i 's/without-password/yes/' /etc/ssh/sshd_config

exit

echo "Evolix partitioning done... You can now reboot!"

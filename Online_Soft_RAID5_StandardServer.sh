#!/bin/bash

# Ce script est une adaptation par jlecour du script de bserie
# Voici les adaptations :
# - /boot fait 500M
# - pas de LVM
# - on n'installe que le paquet virtuel linux-image-amd64 (pas le paquet réel)

echo "ce script ne doit pas être joué automatiquement"
echo "les commandes sont à jouer manuellement, par copier/coller"
exit 1

# Le script suppose que vous avez installé votre dedibox avec le partionnement par défaut.
# /boot sda1 2OOM, / sda2 2G, swap 1G sda3.
# / doit être sur sda2. Sinon adapter le script !
# Enfin un fstab est généré, grub et le kernel sont réinstallés.

ROOT_PART=/dev/md127

export LC_ALL=C
# Dirty hack for rescue Ubuntu 16.04
touch /boot/vmlinuz-pouet
touch /boot/initrd.img-pouet

# disable swap
swapoff -a

cd /mnt

# mount /
mkdir root_in_ram rootfs home var usr log
mount $ROOT_PART /mnt/rootfs/

# copy all the filesystem in ram
mount -t tmpfs none /mnt/root_in_ram -o size=90%
rsync -a /mnt/rootfs/ /mnt/root_in_ram/

# unount rootfs to recreate partitions
umount /mnt/rootfs

# stop soft raid
mdadm --stop /dev/md{125..127}

# Wipe all old signatures
for i in {1..4}; do wipefs -a /dev/sd{a..c}${i}; done

# NOTE: sleep 1 second between each command
#       to be able to copy/paste the whole bloc

# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt
sleep 1
# bios_grub GPT *mandatory*
parted -a minimal -s /dev/sda mkpart primary 0M 1M
sleep 1
parted -s /dev/sda set 1 bios_grub on
sleep 1
# /boot
parted -a minimal -s /dev/sda mkpart primary ext4 1M 500M
sleep 1
parted -s /dev/sda set 2 boot on
parted -s /dev/sda set 2 raid on
sleep 1
# /
parted -a minimal -s /dev/sda mkpart primary ext4 500M 1500M
sleep 1
parted -s /dev/sda set 3 raid on
sleep 1
# /var
parted -a minimal -s /dev/sda mkpart primary ext4 1500M 11500M
sleep 1
parted -s /dev/sda set 4 raid on
sleep 1
# /usr
parted -a minimal -s /dev/sda mkpart primary ext4 11500M 16500M
sleep 1
parted -s /dev/sda set 5 raid on
sleep 1
# /tmp
parted -a minimal -s /dev/sda mkpart primary ext4 16500M 17500M
sleep 1
parted -s /dev/sda set 6 raid on
sleep 1
# SWAP, not in RAID
parted -a minimal -s /dev/sda mkpart primary linux-swap 17500M 18000M
sleep 1
# /home
parted -a minimal -s /dev/sda mkpart primary ext4 18000M 100%
sleep 1
parted -s /dev/sda set 8 raid on
sleep 1

#Copy parts to other disks
for i in {sdb,sdc}; do
    sgdisk -R=/dev/${i} /dev/sda
    sgdisk -G /dev/${i}
done
for i in {sda,sdb,sdc}; do 
    partprobe /dev/${i}
done

# RAID1 for system
for part in {2,3,4,5,6}; do
    mdadm --create /dev/md${part} --metadata=1 --level=raid1 --raid-devices=3 /dev/{sda,sdb,sdc}${part}
done

# RAID5 for data/home
mdadm --create /dev/md8 --metadata=1 --level=raid5 --raid-devices=3 /dev/{sda,sdb,sdc}8

# Make filesystems
mkfs.ext4 -LBOOT /dev/md2 1>/dev/null
mkfs.ext4 -LROOTFS /dev/md3 1>/dev/null
mkfs.ext4 -LVAR /dev/md4 1>/dev/null
mkfs.ext4 -LUSR /dev/md5 1>/dev/null
mkfs.ext4 -LTMP /dev/md6 1>/dev/null
mkswap -f -LSWAP0 /dev/sda7 1>/dev/null
mkswap -f -LSWAP1 /dev/sdb7 1>/dev/null
mkswap -f -LSWAP2 /dev/sdc7 1>/dev/null
mkfs.ext4 -LHOME /dev/md8 1>/dev/null

# mount partitions
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mount -LUSR usr

# Copy data from RAM.
rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/

umount home var usr

# Generate fstab.
cat <<EOT > rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP0     none    swap    sw 0 0
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
EOT

# Chroot + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*

chroot /mnt/rootfs/ bash

export LC_ALL=C
mount /boot
mount /usr
mount /var
mount /tmp
chmod 1777 /tmp

# generate RAID configuration
/usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
apt purge -y grub-common grub-pc grub-pc-bin grub2-common
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
# réinstallation des paquets
# Note: bien installer Grub sur les 3 disques sda/b/c
apt install -y grub-common grub-pc grub-pc-bin grub2-common linux-image-amd64 firmware-linux-nonfree

# paquets utiles pour evolixisation par Ansible
apt install -y sudo python python-apt
# ajouter l'utilisateur dans le groupe sudo
# ça facilite le passage d'Ansible (sans root)
usermod -a -G sudo evolix
passwd evolix

# change root password
passwd
# permit root login via SSH with password
sed -i 's/without-password/yes/' /etc/ssh/sshd_config

exit

echo "Evolix partitioning done... You can now reboot!"

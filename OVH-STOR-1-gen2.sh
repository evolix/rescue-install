#!/bin/bash

# Exécuté avec succès le 2021-11-09 par abenmiloud sur un serveur OVH STOR-1 Gen 2

# Le script suppose que :
# * il y a 2 disques NVMe en RAID logiciel pour le système
# * il y a 4 disques SATA en RAID logiciel
# * le serveur démarre en UEFI
# * on va reconstruire toute la table de partitionnement (GPT) du serveur
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé,
# grub et le kernel sont réinstallés.

# Avant de commencer, consulter les disques pour vérifier les noms
lsblk

# Ne pas tout copier-coller d’un coup !
# Have fun!

if test -d /sys/firmware/efi; then echo 'UEFI'; else echo 'BIOS, fly you fools'; fi

export LC_ALL=C

swapoff -a
sleep 1

cd /mnt
mkdir root_in_ram rootfs home var usr srv

mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md127 /mnt/rootfs
sleep 1

rsync -a /mnt/rootfs/ /mnt/root_in_ram/
sleep 1

umount /mnt/rootfs
mdadm --stop /dev/md127
sleep 1

# Create a GPT label
parted -s /dev/nvme0n1 mklabel gpt
sleep 1
# /boot/efi
parted -a minimal -s /dev/nvme0n1 mkpart ESP fat32 0M 100M
sleep 1
parted -s /dev/nvme0n1 set 1 esp on
sleep 1
parted -s /dev/nvme0n1 set 1 boot on
sleep 1
mkfs.vfat -F32 /dev/nvme0n1p1 >/dev/null
sleep 1
# /boot
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 100M 600M
sleep 1
parted -s /dev/nvme0n1 set 2 boot on
sleep 1
parted -s /dev/nvme0n1 set 2 raid on
sleep 1
# /
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 600M 1600M
sleep 1
parted -s /dev/nvme0n1 set 3 raid on
sleep 1
# /usr
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 1600M 7600M
sleep 1
parted -s /dev/nvme0n1 set 4 raid on
sleep 1
# swap
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 7600M 8100M
sleep 1
parted -s /dev/nvme0n1 set 5 raid on
sleep 1
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 8100M 8600M
sleep 1
parted -s /dev/nvme0n1 set 6 raid on
sleep 1
# LVM: /tmp /var /home
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 8600M 100%
sleep 1
parted -s /dev/nvme0n1 set 7 raid on
sleep 1

sgdisk -R=/dev/nvme1n1 /dev/nvme0n1
sgdisk -G /dev/nvme1n1
sleep 1
partprobe /dev/nvme0n1
partprobe /dev/nvme1n1
sleep 1

for part in 2 3 4 7; do mdadm --create /dev/md"${part}" --metadata=0.90 --level=raid1 --raid-devices=2 /dev/{nvme0n1p,nvme1n1p}"${part}"; done
sleep 1
for part in 2 3 4; do mkfs.ext4 /dev/md"${part}"; done
sleep 1
for part in 5 6; do mkswap -f /dev/nvme0n1p"${part}"; mkswap -f /dev/nvme1n1p"${part}"; done
sleep 1
pvcreate /dev/md7
vgcreate vg0 /dev/md7
lvcreate --size 1G --name tmp vg0
lvcreate --size 5G --name var vg0
lvcreate --size 10G --name home vg0
sleep 1
for lv in tmp var home; do mkfs.ext4 /dev/mapper/vg0-"${lv}"; done

# partie /backup à faire après reboot
wipefs -a /dev/sd{a,b,c,d}
sleep 1

mount /dev/md3 /mnt/rootfs/
mkdir -p /mnt/rootfs/home && mount /dev/vg0/home /mnt/rootfs/home
mkdir -p /mnt/rootfs/var && mount /dev/vg0/var /mnt/rootfs/var
mkdir -p /mnt/rootfs/usr && mount /dev/md4 /mnt/rootfs/usr
sleep 1

rsync -a /mnt/root_in_ram/home/ /mnt/rootfs/home/
rsync -a /mnt/root_in_ram/var/ /mnt/rootfs/var/
rsync -a /mnt/root_in_ram/usr/ /mnt/rootfs/usr/
rsync -a --exclude="home/**" --exclude="var/**" --exclude="usr/**" /mnt/root_in_ram/ /mnt/rootfs/

rm /mnt/rootfs/etc/resolv.conf
echo "nameserver 1.1.1.1" > /mnt/rootfs/etc/resolv.conf

# Reconstruire le fstab avec la sortie de "blkid"
# en utilisant ce modèle :
# UUID=XYZ / ext4 errors=remount-ro 0 1
# UUID=XYZ /boot ext4 defaults 0 2
# UUID=XYZ /boot/efi vfat defaults 0 2
# UUID=XYZ /home ext4 defaults,noexec,nosuid,nodev 0 2
# UUID=XYZ /tmp ext4 defaults,noexec,nosuid,nodev 0 2
# UUID=XYZ /usr ext4 defaults,ro 0 2
# UUID=XYZ /var ext4 defaults,nosuid 0 2
# UUID=XYZ none swap sw 0 2
# UUID=XYZ none swap sw 0 2
# tmpfs /var/tmp tmpfs defaults,noexec,nosuid,nodev,size=1024m 0 0
# tmpfs /dev/shm tmpfs defaults,nodev,nosuid,noexec 0 0

vi /mnt/rootfs/etc/fstab

mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run
sleep 1

rm -rf /mnt/rootfs/boot/*
chroot /mnt/rootfs/ bash
# BEGIN chroot
export LC_ALL=C

mkdir -p /boot/efi && mount /dev/nvme0n1p1 /boot/efi
mount /tmp
chmod 1777 /tmp
rm -rf /etc/default/grub.d

apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
apt install -y lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64 cryptsetup

apt purge -y os-prober
grub-install /dev/nvme0n1
apt purge -y os-prober
update-grub
# Questions "dpkg-reconfigure grub-efi-amd64" :
# * Linux command line:
# * Linux default command line: quiet
# * Force extra installation to the EFI removable media path? Yes
# * Update NVRAM variables to automatically boot into Debian? Yes
dpkg-reconfigure grub-efi-amd64

# copie de /boot/efi
dd if=/dev/nvme0n1p1 of=/dev/nvme1n1p1

# paquets utiles pour evolixisation par Ansible
apt install -y sudo python3 python3-apt

exit
# END chroot

systemctl reboot

# Rescue Install

A collection of scripts to remodel partitions on a freshly installed server (Online/Scaleway or OVH/SoYouStart).

It includes many variants of boot types (BIOS or EFI) and RAID types (hard or soft).

Those scripts and their main structure come from an original implementation by [Benoit Serie](https://gitea.evolix.org/bserie).

## Usage

You should first determine if the server is installed by Online/Scaleway or OVH/SoYouStart.
Then if it has soft RAID or hardware RAID capabilities.
And finaly if th eboot is BIOS or EFI.

Boot your server in rescue mode.

The scripts are not exactly meant to be executed from top to bottom in one go.
They are not smart and the slighest change can break you server.

You should copy/past sections of the script and carefully look for errors in commands output.

Some scripts tend to reuse some partitions (/boot and /boot/efi) but the more recent ones tend to rebuild everything.

The scripts for soft RAID are easily adapted to various RAID configurations.

The scripts for hard RAID are independent of the RAID configuration. The OS doesn't see the physical disks.

## FAQ

### Why sleep after nearly every command?

Some commands are really fast but the kernel needs a moment to commit the changes.
By adding a second of sleep time between commands, we make sure that noone steps on anyone's toe.

### Why remove os-prober?

It messes with the Grub configuration.

### Why does parted give errors about alignment?

No idea! Please investigate and come back with improvments ;)

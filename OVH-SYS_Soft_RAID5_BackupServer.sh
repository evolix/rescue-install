#!/bin/bash

# Script testé (mais erreur) le 2021-06-14 par jlecour
# * Serveur E3-SAT-1-16 - Xeon E3-1225v2 (4c/4th) - 16GB DDR3 1333 MHz - SoftRaid 3x2To SATA

# Le script suppose que :
# * il y a 3 disques en RAID soft
# * le serveur boot en BIOS
# * on va reconstruire toute la table de partitionnement (GPT) du serveur
# * le volume "rootfs" actuel est /dev/md127
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé,
# le volume de backup est préparé/chiffré, grub et le kernel sont réinstallés.

# Avant de commencer, consulter les disques pour vérifier les noms
lsblk

################################################################################
#
# À partir de là, le script *peut* être copié/collé en un bloc
# Mais bien vérifier l'état au fur et à mesure

set -e

export LC_ALL=C

swapoff -a
sleep 1

cd /mnt

mkdir root_in_ram rootfs home var usr srv
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md127 /mnt/rootfs/
sleep 1

rsync -a /mnt/rootfs/ /mnt/root_in_ram/
sleep 1

umount /mnt/rootfs
sleep 1

# Stop arrays and destroys partitions
mdadm --stop /dev/md127
sleep 1

# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt
sleep 1
# bios_grub GPT *mandatory*
parted -a minimal -s /dev/sda mkpart primary 0M 1M
parted -s /dev/sda set 1 bios_grub on
# /boot
parted -a minimal -s /dev/sda mkpart primary ext4 1M 600M
sleep 1
parted -s /dev/sda set 2 boot on
sleep 1
parted -s /dev/sda set 2 raid on
sleep 1
# /
parted -a minimal -s /dev/sda mkpart primary ext4 600M 1100M
sleep 1
parted -s /dev/sda set 3 raid on
sleep 1
# /usr
parted -a minimal -s /dev/sda mkpart primary ext4 1100M 7100M
sleep 1
parted -s /dev/sda set 4 raid on
sleep 1
# swap
parted -a minimal -s /dev/sda mkpart primary ext4 7100M 7600M
sleep 1
# LVM
parted -a minimal -s /dev/sda mkpart primary ext4 7600M 100%
sleep 1
parted -s /dev/sda set 6 raid on
sleep 1

# Copy partitions to second disk
sgdisk -R=/dev/sdb /dev/sda
sleep 1
sgdisk -G /dev/sdb
sleep 1
partprobe /dev/sda
sleep 1
partprobe /dev/sdb
sleep 1

# Copy partitions to third disk
sgdisk -R=/dev/sdc /dev/sda
sleep 1
sgdisk -G /dev/sdc
sleep 1
partprobe /dev/sda
sleep 1
partprobe /dev/sdc
sleep 1

# RAID1 for systems.
# Metadata 0.90 as some OVH kernel need it to boot.
for part in {2,3,4,6}; do
    mdadm --create /dev/md${part} --metadata=0.90 --level=raid5 --raid-devices=3 /dev/{sda,sdb,sdc}${part}
done
sleep 1

mkfs.ext4 -LROOTFS /dev/md3 >/dev/null
sleep 1
mkfs.ext4 -LBOOT /dev/md2 >/dev/null
sleep 1
mkfs.ext4 -LUSR /dev/md4 >/dev/null
sleep 1
mkswap -f -LSWAP1 /dev/sda5
sleep 1
mkswap -f -LSWAP2 /dev/sdb5
sleep 1
mkswap -f -LSWAP3 /dev/sdc5
sleep 1

# Add LVM with a VG for all of the free space.
pvcreate /dev/md6
sleep 1
vgcreate vg0 /dev/md6
sleep 1

lvcreate --size 1G --name tmp vg0
sleep 1
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null
sleep 1

lvcreate --size 5G --name var vg0
sleep 1
mkfs.ext4 -LVAR /dev/mapper/vg0-var >/dev/null
sleep 1

lvcreate --size 10G --name home vg0
sleep 1
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null
sleep 1

apt-get install --yes cryptsetup

lvcreate -l 95%FREE --name backup vg0
sleep 1
cryptsetup --verbose --verify-passphrase luksFormat /dev/mapper/vg0-backup
sleep 1
cryptsetup luksOpen /dev/mapper/vg0-backup backup
sleep 1
mkfs.ext4 -LBACKUP /dev/mapper/backup >/dev/null
sleep 1

# Copy data from RAM.
mount -LROOTFS /mnt/rootfs
mkdir -p /mnt/rootfs/home && mount -LHOME /mnt/rootfs/home
mkdir -p /mnt/rootfs/var && mount -LVAR /mnt/rootfs/var
mkdir -p /mnt/rootfs/usr && mount -LUSR /mnt/rootfs/usr
sleep 1

rsync -a /mnt/root_in_ram/home/ /mnt/rootfs/home/
rsync -a /mnt/root_in_ram/var/ /mnt/rootfs/var/
rsync -a /mnt/root_in_ram/usr/ /mnt/rootfs/usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    /mnt/root_in_ram/ /mnt/rootfs/
sleep 1

# Generate fstab.
cat <<EOT > /mnt/rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 2
LABEL=SWAP2     none    swap    sw 0 2
LABEL=SWAP3     none    swap    sw 0 2
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*
sleep 1

mount -LUSR /mnt/rootfs/usr
sleep 1

chroot /mnt/rootfs/ bash
sleep 1

export LC_ALL=C

mkdir -p /boot && mount /boot
mount /tmp
chmod 1777 /tmp
sleep 1


# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
rm -rf /etc/default/grub.d
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
sleep 1

# purge des kernels
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
sleep 1
# copie de la config RAID soft
/usr/share/mdadm/mkconf > /etc/mdadm.conf
sleep 1
# réinstallation des paquets
apt install -y lvm2 grub-common grub-pc grub-pc-bin grub2-common linux-image-amd64
sleep 1
apt purge -y os-prober

# En cas d'erreur à l'install/config de Grub, il faut vérifier si le RAID est OK
# cat /proc/mdstat

# Questions "update-grub":
# * Linux command line: 
# * Linux default command line: quiet
update-grub
sleep1
dpkg-reconfigure grub-pc
sleep 1

# paquets utiles pour evolixisation par Ansible
apt install -y sudo python python-apt
sleep 1

exit

echo "Evolix partitioning done... You can now reboot!"

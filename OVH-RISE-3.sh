#!/bin/bash

# Script joué le 2021-11-15 par abenmiloud
# * Serveur RISE-3 - Intel Xeon-E 2288G - 8c/16t - 3.7 GHz/5 GHz - 32 GB ECC 2666 MHz - SoftRaid3×4 TB HDD SATA

# Le script suppose que :
# * il y a 3 disques en RAID soft
# * le serveur boot en UEFI
# * on va reconstruire toute la table de partitionnement (GPT) du serveur
# * le volume "rootfs" actuel est /dev/md127
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé,
# le volume de backup sera traitée plus tard grubet le kernel est réinstallé.

# Avant de commencer, consulter les disques pour vérifier les noms
lsblk

################################################################################
#
# À partir de là, le script *peut* être copié/collé en un bloc
# Mais bien vérifier l'état au fur et à mesure

export LC_ALL=C

swapoff -a
sleep 1

cd /mnt

mkdir root_in_ram rootfs home var usr srv
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md127 /mnt/rootfs/
sleep 1

rsync -a /mnt/rootfs/ /mnt/root_in_ram/
sleep 1

umount /mnt/rootfs
sleep 1

mdadm --stop /dev/md127
sleep 1

# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt
sleep 1
# /boot/efi
parted -a minimal -s /dev/sda mkpart ESP fat32 0M 100M
sleep 1
parted -s /dev/sda set 1 esp on
sleep 1
parted -s /dev/sda set 1 boot on
sleep 1
mkfs.vfat -F32 /dev/sda1 > /dev/null
sleep 1
# /boot
parted -a minimal -s /dev/sda mkpart primary ext4 100M 600M
sleep 1
parted -s /dev/sda set 2 boot on
sleep 1
parted -s /dev/sda set 2 raid on
sleep 1
# /
parted -a minimal -s /dev/sda mkpart primary ext4 600M 1600M
sleep 1
parted -s /dev/sda set 3 raid on
sleep 1
# /usr
parted -a minimal -s /dev/sda mkpart primary ext4 1600M 7600M
sleep 1
parted -s /dev/sda set 4 raid on
sleep 1
# swap
parted -a minimal -s /dev/sda mkpart primary ext4 7600M 8100M
sleep 1
parted -a minimal -s /dev/sda mkpart primary ext4 8100M 8600M
sleep 1
# LVM
parted -a minimal -s /dev/sda mkpart primary ext4 8600M 100%
sleep 1
parted -s /dev/sda set 7 raid on
sleep 1

# Copy partitions to second disk
sgdisk -R=/dev/sdb /dev/sda
sleep 1
sgdisk -G /dev/sdb
sleep 1
partprobe /dev/sda
sleep 1
partprobe /dev/sdb
sleep 1

# Copy partitions to third disk
sgdisk -R=/dev/sdc /dev/sda
sleep 1
sgdisk -G /dev/sdc
sleep 1
partprobe /dev/sda
sleep 1
partprobe /dev/sdc
sleep 1

# RAID5 for systems.
# Metadata 0.90 as some OVH kernel need it to boot.
for part in 2 3 4 7
do
    mdadm --create /dev/md"${part}" --metadata=0.90 --level=raid5 --raid-device=3 /dev/{sda,sdb,sdc}"${part}"
done
sleep 1

mkfs.ext4 /dev/md2 > /dev/null
sleep 1
mkfs.ext4 /dev/md3 > /dev/null
sleep 1
mkfs.ext4 /dev/md4 > /dev/null
sleep 1
for disk in sda sdb sdc
do
    for part in 5 6
    do
        mkswap -f /dev/"${disk}""${part}"
        sleep 1
    done
done

# Add LVM with a VG for all of the free space.
pvcreate /dev/md7
sleep 1
vgcreate vg0 /dev/md7
sleep 1

lvcreate --size 1G --name tmp vg0
sleep 1
mkfs.ext4 /dev/mapper/vg0-tmp > /dev/null
sleep 1

lvcreate --size 5G --name var vg0
sleep 1
mkfs.ext4 /dev/mapper/vg0-var > /dev/null
sleep 1

lvcreate --size 10G --name home vg0
sleep 1
mkfs.ext4 /dev/mapper/vg0-home > /dev/null
sleep 1

# Copy data from RAM.
mount /dev/md3 /mnt/rootfs
mkdir -p /mnt/rootfs/home && mount /dev/mapper/vg0-home /mnt/rootfs/home
mkdir -p /mnt/rootfs/var && mount /dev/mapper/vg0-var /mnt/rootfs/var
mkdir -p /mnt/rootfs/usr && mount /dev/md4 /mnt/rootfs/usr
mkdir -p /mnt/rootfs/boot && mount /dev/md2 /mnt/rootfs/boot
mkdir -p /mnt/rootfs/boot/efi && mount /dev/sda1 /mnt/rootfs/boot/efi
mkdir -p /mnt/rootfs/tmp && mount /dev/mapper/vg0-tmp /mnt/rootfs/tmp
sleep 1

rsync -a /mnt/root_in_ram/home/ /mnt/rootfs/home/
rsync -a /mnt/root_in_ram/var/ /mnt/rootfs/var/
rsync -a /mnt/root_in_ram/usr/ /mnt/rootfs/usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    --exclude="boot/**" \
    /mnt/root_in_ram/ /mnt/rootfs/
sleep 1

# Generate fstab.
cat <<EOT > /mnt/rootfs/etc/fstab
UUID=… /boot/efi vfat defaults 0 2
UUID=… / ext4 errors=remount-ro 0 1
UUID=… /boot ext4 defaults 0 2
UUID=… /home ext4 defaults 0 2
UUID=… /tmp ext4 defaults 0 2
UUID=… /usr ext4 defaults 0 2
UUID=… /var ext4 defaults 0 2
UUID=… none swap sw 0 2
UUID=… none swap sw 0 2
UUID=… none swap sw 0 2
UUID=… none swap sw 0 2
UUID=… none swap sw 0 2
UUID=… none swap sw 0 2
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

chroot /mnt/rootfs/ bash
sleep 1

# BEGIN chroot
export LC_ALL=C

chmod 1777 /tmp
sleep 1

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
rm -rf /etc/default/grub.d
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
sleep 1

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
rm -rf /etc/default/grub.d
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
sleep 1

# purge des kernels
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }')
do
    apt purge -y "${kernel_pkg}"
done
sleep 1
# copie de la config RAID soft
/usr/share/mdadm/mkconf > /etc/mdadm.conf
sleep 1
# réinstallation des paquets
apt install -y lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64
sleep 1
apt purge -y os-prober

dd if=/dev/sda1 of=/dev/sdb1
dd if=/dev/sda1 of=/dev/sdc1
efibootmgr -c -g -d /dev/sdb -p 1 -L debian -l '\EFI\debian\grubx64.efi'
efibootmgr -c -g -d /dev/sdc -p 1 -L debian -l '\EFI\debian\grubx64.efi'

grub-install /dev/sda
sleep 1
grub-install /dev/sdb
sleep 1
grub-install /dev/sdc
sleep 1

# Questions "update-grub":
# * Linux command line:
# * Linux default command line: quiet
# * Force extra installation to the EFI removable media path? Yes
# * Update NVRAM variables to automatically boot into Debian? Yes
update-grub
dpkg-reconfigure grub-efi-amd64
sleep 1

passwd debian
vi /etc/ssh/sshd_config
systemctl reload ssh.service

exit
# END chroot

echo "Evolix partitioning done... You can now reboot!"

#!/bin/bash
 
# Le script suppose que :
# * il y a un (seul) ensemble RAID hard
# * le serveur boot en EFI
# * on va reconstruire toute la table de partitionnement (GPT) du serveur
# * le volume "rootfs" actuel est /dev/sda2
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé,
# grub et le kernel sont réinstallés.

# Si vous utilisez un terminal exotique, émulez xterm
export TERM=xterm

# Avant de commencer, consulter les disques pour vérifier les noms
lsblk

# utilisez "set -e" si vous copiez/collez des bouts de scripts et souhaitez
# un arrêt en cas d'erreur. Si vous le faites dans le shell de votre session SSH
# elle s'interrompra !
# set -e

export LC_ALL=C

swapoff -a
sleep 1

cd /mnt

mkdir root_in_ram rootfs home var usr srv
mount -t tmpfs none /mnt/root_in_ram -o size=90%

mount /dev/sda3 /mnt/rootfs/
sleep 1

rsync -a /mnt/rootfs/ /mnt/root_in_ram/
sleep 1

umount /mnt/rootfs
sleep 1

# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt
sleep 1

# BOOT EFI *mandatory* 100 Mo
parted -a minimal -s /dev/sda mkpart ESP fat32 0M 100M
sleep 1
parted -s /dev/sda set 1 esp on
sleep 1
parted -s /dev/sda set 1 boot on
sleep 1
mkfs.vfat -F32 /dev/sda1 >/dev/null
sleep 1
fatlabel /dev/sda1 EFI_SYSPART
sleep 1

# /boot 500M
parted -a minimal -s /dev/sda mkpart primary ext4 100M 600M
sleep 1
parted -s /dev/sda set 2 boot on
sleep 1
mkfs.ext4 -LBOOT /dev/sda2 >/dev/null
sleep 1

# / 1G
parted -a optimal -s /dev/sda mkpart primary ext4 600M 1600M
sleep 1
mkfs.ext4 -LROOTFS /dev/sda3 >/dev/null
sleep 1

# /usr 6G
parted -a optimal -s /dev/sda mkpart primary ext4 1600M 7600M
sleep 1
mkfs.ext4 -LUSR /dev/sda4 >/dev/null
sleep 1

# swap1 500M
parted -a optimal -s /dev/sda mkpart primary ext4 7600M 8100M
sleep 1
mkswap -f -LSWAP1 /dev/sda5 >/dev/null
sleep 1

# swap2 500M
parted -a optimal -s /dev/sda mkpart primary ext4 8100M 8600M
sleep 1
mkswap -f -LSWAP2 /dev/sda6 >/dev/null
sleep 1

# LVM
parted -a minimal -s /dev/sda mkpart primary ext4 8600M 100%
sleep 1

# Add LVM with a VG for all of the free space.
pvcreate /dev/sda7
sleep 1
vgcreate HDD /dev/sda7
sleep 1

# /tmp 1G
lvcreate --yes --size 1G --name tmp HDD
sleep 1
mkfs.ext4 -LTMP /dev/mapper/HDD-tmp >/dev/null
sleep 1

# /var 10G
lvcreate --yes --size 10G --name var HDD
sleep 1
mkfs.ext4 -LVAR /dev/mapper/HDD-var >/dev/null
sleep 1

# /home 10G
lvcreate --yes --size 10G --name home HDD
sleep 1
mkfs.ext4 -LHOME /dev/mapper/HDD-home >/dev/null
sleep 1

apt-get install cryptsetup

# /backup 90%
lvcreate --yes -l 90%FREE --name backup HDD
sleep 1
cryptsetup --verbose --verify-passphrase luksFormat /dev/mapper/HDD-backup
sleep 1
cryptsetup luksOpen /dev/mapper/HDD-backup backup
sleep 1
mkfs.ext4 -LBACKUP /dev/mapper/backup >/dev/null
sleep 1

# Copy data from RAM.
mount -LROOTFS rootfs
mkdir -p rootfs/home && mount -LHOME rootfs/home
mkdir -p rootfs/var && mount -LVAR rootfs/var
mkdir -p rootfs/usr && mount -LUSR rootfs/usr
sleep 1

rsync -a root_in_ram/home/ rootfs/home/
rsync -a root_in_ram/var/ rootfs/var/
rsync -a root_in_ram/usr/ rootfs/usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/
sleep 1

# Generate fstab.
cat <<EOT >rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=EFI_SYSPART /boot/efi   vfat   defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount -o bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*
sleep 1

chroot /mnt/rootfs/ bash
sleep 1

export LC_ALL=C

mkdir -p /boot && mount /boot
mkdir -p /boot/efi && mount /dev/sda1 /boot/efi
mount /tmp
chmod 1777 /tmp
sleep 1

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
rm -rf /etc/default/grub.d
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
sleep 1

# purge des kernels
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
sleep 1
# réinstallation des paquets
apt install -y cryptsetup lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64
sleep 1
apt purge -y os-prober

# En cas d'erreur à l'install/config de Grub, il faut vérifier si le RAID est OK
# cat/proc/mdstat

grub-install /dev/sda
sleep 1
apt purge -y os-prober

# Questions "update-grub":
# * Linux command line: 
# * Linux default command line: quiet
# * Force extra installation to the EFI removable media path? Yes
# * Update NVRAM variables to automatically boot into Debian? Yes
update-grub
# dpkg-reconfigure --frontend=noninteractive grub-efi-amd64
dpkg-reconfigure grub-efi-amd64
sleep 1
#!/bin/bash

# Le script suppose que :
# * il y a 3 disques en RAID soft
# * le serveur boot en EFI
# * on va reconstruire toute la table de partitionnement (GPT) du serveur
# * le volume "rootfs" actuel est /dev/md127
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé,
# grub et le kernel sont réinstallés.

# vérifié par jlecour le 2021-08-27 sur OVH INFRA-2

# Avant de commencer, consulter les disques pour vérifier les noms
lsblk

################################################################################
#
# À partir de là, le script *peut* être copié/collé en un bloc
# Mais bien vérifier l'état au fur et à mesure

set -e

export LC_ALL=C

swapoff -a
sleep 1

cd /mnt

mkdir root_in_ram rootfs home var usr srv
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md127 /mnt/rootfs/
sleep 1

rsync -a /mnt/rootfs/ /mnt/root_in_ram/
sleep 1

umount /mnt/rootfs
sleep 1

# Stop arrays and destroys partitions
mdadm --stop /dev/md127
sleep 1

# Create a GPT label. (Removes all parts).
parted -s /dev/nvme0n1 mklabel gpt
sleep 1
# BOOT EFI *mandatory* 100 Mo
parted -a minimal -s /dev/nvme0n1 mkpart ESP fat32 0M 100M
sleep 1
parted -s /dev/nvme0n1 set 1 esp on
sleep 1
parted -s /dev/nvme0n1 set 1 boot on
sleep 1
mkfs.vfat -F32 /dev/nvme0n1p1 >/dev/null
sleep 1
fatlabel /dev/nvme0n1p1 EFI_SYSPART
sleep 1
# /boot
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 100M 600M
sleep 1
parted -s /dev/nvme0n1 set 2 boot on
sleep 1
parted -s /dev/nvme0n1 set 2 raid on
sleep 1
# /
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 600M 1600M
sleep 1
parted -s /dev/nvme0n1 set 3 raid on
sleep 1
# /usr
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 1600M 7600M
sleep 1
parted -s /dev/nvme0n1 set 4 raid on
sleep 1
# swap
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 7600M 8100M
sleep 1
# LVM
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 8100M 100%
sleep 1
parted -s /dev/nvme0n1 set 6 raid on
sleep 1

# Copy partitions to second disk
sgdisk -R=/dev/nvme1n1 /dev/nvme0n1
sleep 1
sgdisk -G /dev/nvme1n1
sleep 1
partprobe /dev/nvme0n1
sleep 1
partprobe /dev/nvme1n1
sleep 1

# Copy partitions to third disk
sgdisk -R=/dev/nvme2n1 /dev/nvme0n1
sleep 1
sgdisk -G /dev/nvme2n1
sleep 1
partprobe /dev/nvme0n1
sleep 1
partprobe /dev/nvme2n1
sleep 1

# RAID1 for systems.
# Metadata 0.90 as some OVH kernel need it to boot.
for part in {2,3,4,6}; do
    mdadm --create /dev/md${part} --metadata=0.90 --level=raid5 --raid-devices=3 /dev/{nvme0n1p,nvme1n1p,nvme2n1p}${part}
done
sleep 1

mkfs.ext4 -LROOTFS /dev/md3 >/dev/null
sleep 1
mkfs.ext4 -LBOOT /dev/md2 >/dev/null
sleep 1
mkfs.ext4 -LUSR /dev/md4 >/dev/null
sleep 1
mkswap -f -LSWAP1 /dev/nvme0n1p5
sleep 1
mkswap -f -LSWAP2 /dev/nvme1n1p5
sleep 1
mkswap -f -LSWAP3 /dev/nvme2n1p5
sleep 1

# Add LVM with a VG for all of the free space.
pvcreate /dev/md6
sleep 1
vgcreate vg0 /dev/md6
sleep 1

lvcreate --size 1G --name tmp vg0
sleep 1
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null
sleep 1

lvcreate --size 5G --name var vg0
sleep 1
mkfs.ext4 -LVAR /dev/mapper/vg0-var >/dev/null
sleep 1

lvcreate -l 98%FREE --name home vg0
sleep 1
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null
sleep 1

# Copy data from RAM.
mount -LROOTFS rootfs
mkdir -p rootfs/home && mount -LHOME rootfs/home
mkdir -p rootfs/var && mount -LVAR rootfs/var
mkdir -p rootfs/usr && mount -LUSR rootfs/usr
sleep 1

rsync -a root_in_ram/home/ rootfs/home/
rsync -a root_in_ram/var/ rootfs/var/
rsync -a root_in_ram/usr/ rootfs/usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/
sleep 1

# Generate fstab.
cat <<EOT >rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=EFI_SYSPART /boot/efi   vfat   defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 2
LABEL=SWAP2     none    swap    sw 0 2
LABEL=SWAP3     none    swap    sw 0 2
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*
sleep 1

chroot /mnt/rootfs/ bash
sleep 1

export LC_ALL=C

mkdir -p /boot && mount /boot
mkdir -p /boot/efi && mount /dev/nvme0n1p1 /boot/efi
mount /tmp
chmod 1777 /tmp
sleep 1


# apt install -y debconf-utils apt-utils
# sleep 1
# cat <<EOT>/tmp/grub-selections
# grub-efi-amd64    grub2/force_efi_extra_removable    boolean    true
# grub-efi-amd64    grub2/linux_cmdline_default    string    quiet
# grub-efi-amd64    grub2/update_nvram    boolean    true
# grub-efi-amd64    grub2/linux_cmdline    string
# grub-pc    grub-pc/postrm_purge_boot_grub    boolean    true
# EOT
# sleep 1
# debconf-set-selections /tmp/grub-selections
# sleep 1
# dpkg-reconfigure --frontend=noninteractive grub-pc
# sleep 1

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
rm -rf /etc/default/grub.d
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
sleep 1

# purge des kernels
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
sleep 1
# réinstallation des paquets
apt install -y lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64
sleep 1
apt purge -y os-prober

# En cas d'erreur à l'install/config de Grub, il faut vérifier si le RAID est OK
# cat/proc/mdstat

grub-install /dev/nvme0n1
sleep 1
grub-install /dev/nvme1n1
sleep 1
grub-install /dev/nvme2n1
sleep 1

# Questions "update-grub":
# * Linux command line: 
# * Linux default command line: quiet
# * Force extra installation to the EFI removable media path? Yes
# * Update NVRAM variables to automatically boot into Debian? Yes
update-grub
# dpkg-reconfigure --frontend=noninteractive grub-efi-amd64
dpkg-reconfigure grub-efi-amd64

sleep 1

# paquets utiles pour evolixisation par Ansible
apt install -y sudo python python-apt
sleep 1

exit

echo "Evolix partitioning done... You can now reboot!"

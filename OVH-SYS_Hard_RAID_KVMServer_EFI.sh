#!/bin/bash
 
# Le script suppose que vous avez installé votre serveur avec le partionnement par défaut.
# / doit être sur /dev/sda2. Sinon adapter le script !
# Enfin un fstab est généré, lvm2 est installé, grub et le kernel sont réinstallés.

export LC_ALL=C

swapoff -a

cd /mnt

mkdir root_in_ram rootfs home var usr log
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/sda2 /mnt/rootfs/

rsync -a /mnt/rootfs/ /mnt/root_in_ram/

umount /mnt/rootfs
# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt
sleep 2
# BOOT EFI *mandatory* 100 Mo
parted -a minimal -s /dev/sda mkpart ESP fat32 0M 100M
parted -s /dev/sda set 1 esp on
parted -s /dev/sda set 1 boot on
mkfs.vfat -F32 /dev/sda1 >/dev/null
fatlabel /dev/sda1 EFI_SYSPART
sleep 2
# /boot 500 Mo
parted -a minimal -s /dev/sda mkpart primary ext4 100M 600M
mkfs.ext4 -LBOOT /dev/sda2 >/dev/null
sleep 2
# / 500 Mo
parted -a optimal -s /dev/sda mkpart primary ext4 600M 1100M
mkfs.ext4 -LROOTFS /dev/sda3 >/dev/null
sleep 2
# /usr 6 Go
parted -a optimal -s /dev/sda mkpart primary ext4 1100M 7100M
mkfs.ext4 -LUSR /dev/sda4 >/dev/null
sleep 2
# swap1 10 Go
parted -a minimal -s /dev/sda mkpart primary ext4 7100M 17100M
mkswap -f -LSWAP1 /dev/sda5
sleep 2
# swap2 10 Go
parted -a minimal -s /dev/sda mkpart primary ext4 17100M 27100M
mkswap -f -LSWAP2 /dev/sda6
sleep 2
# LVM 100%
parted -a minimal -s /dev/sda mkpart primary ext4 27100M 100%
parted -s /dev/sda set 7 lvm on
sleep 2
# Add LVM with a VG for all of the free space.
pvcreate /dev/sda7
vgcreate vg0 /dev/sda7
sleep 2
# TMP
lvcreate --size 1G --name tmp vg0
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null
sleep 2
# VAR
lvcreate --size 5G --name var vg0
mkfs.ext4 -LVAR /dev/mapper/vg0-var >/dev/null
sleep 2
# HOME
lvcreate --size 50G --name home vg0
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null
sleep 2

# Le reste du disque est laissé tel quel

# Mount partitions
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mkdir rootfs/usr
mount -LUSR rootfs/usr

# Copy data from RAM.
rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ rootfs/usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/

umount home var
# si on démonte usr, le chroot n'est pas possible (bash n'est pas trouvé)
# umount rootfs/usr

# Generate fstab.
cat <<EOT>rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=EFI_SYSPART /boot/efi   vfat   defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 2
LABEL=SWAP2     none    swap    sw 0 2
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*

chroot /mnt/rootfs/ bash

export LC_ALL=C

mount /boot
mount /boot/efi
# /usr est déjà monté depuis l'extérieur du chroot
mount /var
mount /tmp
chmod 1777 /tmp

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
# * Remove GRUB 2 from /boot/grub? Yes
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common

for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done

# réinstallation des paquets
apt install -y lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64

grub-install /dev/sda

# Questions "update-grub":
# * Linux command line: 
# * Linux default command line: quiet
# * Force extra installation to the EFI removable media path? Yes
# * Update NVRAM variables to automatically boot into Debian? Yes
update-grub

dpkg-reconfigure grub-efi-amd64


# paquets utiles pour evolixisation par Ansible
apt install -y sudo python python-apt

passwd
sed -i 's/without-password/yes/' /etc/ssh/sshd_config
exit

echo "Evolix partitioning done... You can now reboot!"

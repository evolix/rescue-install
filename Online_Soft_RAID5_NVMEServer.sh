#!/bin/bash

echo "ce script ne doit pas être joué automatiquement"
echo "les commandes sont à jouer manuellement, par copier/coller"
exit 1

# Le script suppose que vous avez installé votre dedibox avec le partionnement par défaut.
# UEFI p1 fait 6OOM, /boot p2 fait 600M, SWAP p3, / p4.
# / doit être sur md127. Sinon adapter le script !
# Enfin un fstab est généré, grub et le kernel sont réinstallés.

ROOT_PART=/dev/md127
export LC_ALL=C
# disable swap
swapoff -a
cd /mnt

# mount /
mkdir root_in_ram rootfs home var usr log
mount $ROOT_PART /mnt/rootfs/

# copy all the filesystem in ram
mount -t tmpfs none /mnt/root_in_ram -o size=90%
rsync -a /mnt/rootfs/ /mnt/root_in_ram/

# unount rootfs to recreate partitions
umount /mnt/rootfs

# stop soft raid
mdadm --stop /dev/md{125..127}
# Wipe all signatures
for i in /dev/nvme?n?p?; do wipefs -a $i; done

# NOTE: sleep 1 second between each command
#       to be able to copy/paste the whole bloc

# Create a GPT label. (Removes all parts).
parted -s /dev/nvme0n1 mklabel gpt
sleep 1
# boot,esp GPT/UEFI *mandatory*
parted -a minimal -s /dev/nvme0n1 mkpart primary 0M 600M
sleep 1
parted -s /dev/nvme0n1 set 1 boot on
parted -s /dev/nvme0n1 set 1 esp on
sleep 1
# /boot
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 600M 1200M
sleep 1
parted -s /dev/nvme0n1 set 2 raid on
sleep 1
# /
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 1200M 2200M
sleep 1
parted -s /dev/nvme0n1 set 3 raid on
sleep 1
# /var
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 2200M 12200M
sleep 1
parted -s /dev/nvme0n1 set 4 raid on
sleep 1
# /usr
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 12200M 17200M
sleep 1
parted -s /dev/nvme0n1 set 5 raid on
sleep 1
# /tmp
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 17200M 18200M
sleep 1
parted -s /dev/nvme0n1 set 6 raid on
sleep 1
# swap1
parted -a minimal -s /dev/nvme0n1 mkpart primary linux-swap 18200M 19200M
sleep 1
# /home
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 19200M 100%
sleep 1
parted -s /dev/nvme0n1 set 8 raid on
sleep 1

#Copy parts to nvme1n1
for i in /dev/nvme[123]n1; do
    sgdisk -R=${i} /dev/nvme0n1
    sgdisk -G $i
    partprobe $i
done

# RAID1 for systems.
for part in {2,3,4,5,6}; do
    mdadm --create /dev/md${part} --metadata=1.2 --level=raid1 --raid-devices=4 \
      /dev/{nvme0n1p${part},nvme1n1p${part},nvme2n1p${part},nvme3n1p${part}}
done

# RAID5 for data.
mdadm --create /dev/md8 --metadata=1.2 --level=raid5 --raid-devices=4 \
  /dev/{nvme0n1p8,nvme1n1p8,nvme2n1p8,nvme3n1p8}

# Make filesystems
l=1
for i in /dev/nvme?n1p[7]; do 
    mkswap -f -LSWAP${l} $i 1>/dev/null
    l=$((l+1))
done

apt install dosfstools
for i in /dev/nvme?n1p[1]; do 
    mkfs.vfat $i 1>/dev/null
done

mkfs.ext4 -LBOOT /dev/md2 1>/dev/null
mkfs.ext4 -LROOTFS /dev/md3 1>/dev/null
mkfs.ext4 -LVAR /dev/md4 1>/dev/null
mkfs.ext4 -LUSR /dev/md5 1>/dev/null
mkfs.ext4 -LTMP /dev/md6 1>/dev/null
mkfs.ext4 -LHOME /dev/md9 1>/dev/null

# mount partitions
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mount -LUSR usr

# Copy data from RAM.
rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/

umount home var usr

# Generate fstab.
cat <<EOT > rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
/dev/nvme0n1p1  /boot/efi vfat  defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
LABEL=SWAP3     none    swap    sw 0 0
LABEL=SWAP4     none    swap    sw 0 0
LABEL=SWAP5     none    swap    sw 0 0
LABEL=SWAP6     none    swap    sw 0 0
LABEL=SWAP7     none    swap    sw 0 0
LABEL=SWAP8     none    swap    sw 0 0
EOT

# Chroot + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*

chroot /mnt/rootfs/ bash

export LC_ALL=C
mount /boot
mount /boot/efi
mount /usr
mount /var
mount /tmp
chmod 1777 /tmp

# generate RAID configuration
/usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
apt purge -y grub-common grub-efi-amd64 grub-efi-amd64-bin grub2-common
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
# réinstallation des paquets
# Note: bien installer Grub sur les 4 disques nvme[0123]n1
apt install -y grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64
grub-install /dev/nvme0n1
update-grub
dpkg-reconfigure grub-efi-amd64


# paquets utiles pour evolixisation par Ansible
apt install -y sudo python python-apt
# ajouter l'utilisateur dans le groupe sudo
# ça facilite le passage d'Ansible (sans root)
usermod -a -G sudo jlecour
passwd jlecour

# change root password
passwd
# permit root login via SSH with password
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

exit

echo "Evolix partitioning done... You can now reboot!"

#!/bin/bash

# Script vérifié le 2021-03-29 par jlecour

# Le script suppose que :
# * il y a 1 disque NVMe
# * il y a 4 disques SATA en RAID soft
# * le serveur boot en EFI
# * on va reconstruire toute la table de partitionnement (GPT) du serveur
# * le volume "rootfs" actuel est /dev/md127
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé,
# grub et le kernel sont réinstallés.

# Avant de commencer, consulter les disques pour vérifier les noms
lsblk

################################################################################
#
# À partir de là, le script *peut* être copié/collé en un bloc
# Mais bien vérifier l'état au fur et à mesure

# utilisez "set -e" si vous copiez/collez des bouts de scripts et souhaitez
# un arrêt en cas d'erreur. Si vous le faites dans le shell de votre session SSH
# elle s'interrompra !
# set -e

export LC_ALL=C

swapoff -a
sleep 1

cd /mnt

mkdir root_in_ram rootfs home var usr srv
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md127 /mnt/rootfs/
sleep 1

rsync -a /mnt/rootfs/ /mnt/root_in_ram/
sleep 1

umount /mnt/rootfs
sleep 1

# Stop arrays and destroys partitions
mdadm --stop /dev/md127
sleep 1

# Create a GPT label. (Removes all parts).
parted -s /dev/nvme0n1 mklabel gpt
sleep 1
# BOOT EFI *mandatory* 100 Mo
parted -a minimal -s /dev/nvme0n1 mkpart ESP fat32 0M 100M
sleep 1
parted -s /dev/nvme0n1 set 1 esp on
sleep 1
parted -s /dev/nvme0n1 set 1 boot on
sleep 1
mkfs.vfat -F32 /dev/nvme0n1p1 >/dev/null
sleep 1
fatlabel /dev/nvme0n1p1 EFI_SYSPART
sleep 1
# /boot
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 100M 600M
sleep 1
parted -s /dev/nvme0n1 set 2 boot on
sleep 1
# /
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 600M 1600M
sleep 1
# /usr
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 1600M 7600M
sleep 1
# swap
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 7600M 8100M
sleep 1
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 8100M 8600M
sleep 1
# LVM
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 8600M 100%
sleep 1

# Backup
parted -s /dev/sda mklabel gpt
parted -a minimal -s /dev/sda mkpart primary ext4 0 100%
parted -s /dev/sda set 1 raid on

# Copy partitions to second disk
sgdisk -R=/dev/sdb /dev/sda
sleep 1
sgdisk -G /dev/sdb

# Copy partitions to third disk
sgdisk -R=/dev/sdc /dev/sda
sleep 1
sgdisk -G /dev/sdc
sleep 1

# Copy partitions to fourth disk
sgdisk -R=/dev/sdd /dev/sda
sleep 1
sgdisk -G /dev/sdd
sleep 1

# RAID6 for /backup
mdadm --create /dev/md0 --level=6 --raid-devices=4 /dev/sda1 /dev/sdb1 /dev/sdc1 /dev/sdd1
sleep 1

mkfs.ext4 -LROOTFS /dev/nvme0n1p3 >/dev/null
sleep 1
mkfs.ext4 -LBOOT /dev/nvme0n1p2 >/dev/null
sleep 1
mkfs.ext4 -LUSR /dev/nvme0n1p4 >/dev/null
sleep 1
mkswap -f -LSWAP1 /dev/nvme0n1p5
sleep 1
mkswap -f -LSWAP2 /dev/nvme0n1p6
sleep 1

# Add LVM with a VG for all of the free space.
pvcreate /dev/nvme0n1p7
sleep 1
vgcreate vg0 /dev/nvme0n1p7
sleep 1

lvcreate --size 1G --name tmp vg0
sleep 1
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null
sleep 1

lvcreate --size 10G --name var vg0
sleep 1
mkfs.ext4 -LVAR /dev/mapper/vg0-var >/dev/null
sleep 1

lvcreate -l 50%FREE --name home vg0
sleep 1
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null
sleep 1

# LUKS for /backup
apt install cryptsetup

cryptsetup --verbose --verify-passphrase luksFormat /dev/md0
sleep 1
cryptsetup luksOpen /dev/md0 backup
sleep 1
mkfs.ext4 -LBACKUP /dev/mapper/backup >/dev/null
sleep 1

# Copy data from RAM.
mount -LROOTFS rootfs
mkdir -p rootfs/home && mount -LHOME rootfs/home
mkdir -p rootfs/var && mount -LVAR rootfs/var
mkdir -p rootfs/usr && mount -LUSR rootfs/usr
sleep 1

rsync -a root_in_ram/home/ rootfs/home/
rsync -a root_in_ram/var/ rootfs/var/
rsync -a root_in_ram/usr/ rootfs/usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/
sleep 1

# Generate fstab.
cat <<EOT > rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=EFI_SYSPART /boot/efi   vfat   defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 2
LABEL=SWAP2     none    swap    sw 0 2
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*
sleep 1

chroot /mnt/rootfs/ bash
sleep 1

export LC_ALL=C

mkdir -p /boot && mount /boot
mkdir -p /boot/efi && mount /dev/nvme0n1p1 /boot/efi
mount /tmp
chmod 1777 /tmp
sleep 1

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
rm -rf /etc/default/grub.d
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
sleep 1

# purge des kernels
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
sleep 1
# réinstallation des paquets
apt install -y lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64
sleep 1
apt purge -y os-prober

grub-install /dev/nvme0n1
sleep 1
apt purge -y os-prober

# Questions "update-grub":
# * Linux command line: 
# * Linux default command line: quiet
# * Force extra installation to the EFI removable media path? Yes
# * Update NVRAM variables to automatically boot into Debian? Yes
update-grub
# dpkg-reconfigure --frontend=noninteractive grub-efi-amd64
dpkg-reconfigure grub-efi-amd64
sleep 1

# paquets utiles pour evolixisation par Ansible
apt install -y sudo python python-apt
sleep 1

exit

echo "Evolix partitioning done... You can now reboot!"

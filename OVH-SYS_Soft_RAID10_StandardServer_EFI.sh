#!/bin/bash
 
# Le script suppose que vous avez installé votre serveur avec le partionnement par défaut.
# /dev/md3 /, /dev/md4 /home.
# / doit être sur /dev/md3. Sinon adapter le script !
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé, grub et le kernel sont réinstallés.

export LC_ALL=C
swapoff -a
cd /mnt
mkdir root_in_ram rootfs home var usr
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md3 /mnt/rootfs/
rsync -a /mnt/rootfs/ /mnt/root_in_ram/
umount /mnt/rootfs
# Change LABEL for /boot, yeah 2 times, IDK why but the first time is not applied Oo
mdadm --stop /dev/md4
mdadm --stop /dev/md3
tune2fs -LBOOT /dev/md2 && tune2fs -LBOOT /dev/md2
# Check if label is BOOT
wipefs /dev/md2

mdadm --stop /dev/md2
parted -s /dev/sda rm 5
parted -s /dev/sda rm 4
parted -s /dev/sda rm 3
# /
parted -a optimal -s /dev/sda mkpart primary ext4 1073M 2073M
sleep 2
parted -s /dev/sda set 3 raid on
# /var
parted -a minimal -s /dev/sda mkpart primary ext4 2073M 12073M
sleep 2
parted -s /dev/sda set 4 raid on
# /usr
parted -a minimal -s /dev/sda mkpart primary ext4 12073M 17073M
sleep 2
parted -s /dev/sda set 5 raid on
# LVM
parted -a minimal -s /dev/sda mkpart primary ext4 17073M 100%
parted -s /dev/sda set 6 raid on
sleep 2

#Copy parts to sd{b,c,d,e,f}
for i in sd{b,c,d,e,f}; do 
    sgdisk -R=/dev/$i /dev/sda
    sgdisk -G /dev/$i
    partprobe /dev/$i
done

mdadm --assemble /dev/md2 /dev/{sda,sdb,sdc,sdd,sde,sdf}2

# RAID1 for systems.
# Metadata 0.90 as some OVH kernel need it to boot.
for part in {3,4,5}; do
    mdadm --create /dev/md${part} --metadata=0.90 --level=raid1 --raid-devices=6 /dev/{sda,sdb,sdc,sdd,sde,sdf}${part}
done
# No need for LVM part to has metadata 0.90.
yes | mdadm --create /dev/md6 --level=raid10 --raid-devices=6 /dev/{sda,sdb,sdc,sdd,sde,sdf}6

mkfs.ext4 -LROOTFS /dev/md3 >/dev/null
mkfs.ext4 -LVAR /dev/md4 >/dev/null
mkfs.ext4 -LUSR /dev/md5 >/dev/null

# Add LVM with a VG for all of the free space.
pvcreate /dev/md6
vgcreate vg0 /dev/md6
lvcreate --size 1G --name tmp vg0
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null
lvcreate --size 512M --name swap1 vg0
lvcreate --size 512M --name swap2 vg0
mkswap -f -LSWAP1 /dev/mapper/vg0-swap1
mkswap -f -LSWAP2 /dev/mapper/vg0-swap2
lvcreate --extents 95%FREE --name home vg0
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null

# Copy data from RAM.
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mount -LUSR usr
rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/
umount home var usr

# Generate fstab.
cat <<EOT>rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=EFI_SYSPART /boot/efi   vfat   defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run
rm -rf /mnt/rootfs/boot/*
chroot /mnt/rootfs/ bash
export LC_ALL=C
mount /boot
mount /dev/sda1 /boot/efi
mount /usr
mount /var
mount /tmp
chmod 1777 /tmp
apt purge -y grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
# LVM2 déjà OK sur machines OVH
#apt install -y lvm2
/usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf
apt install -y grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64 dosfstools
update-grub2
passwd
sed -i 's/without-password/yes/' /etc/ssh/sshd_config
exit

echo "Evolix partitioning done... You can now reboot!"

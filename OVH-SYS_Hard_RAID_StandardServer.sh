#!/bin/bash
 
# Le script suppose que vous avez installé votre serveur avec le partionnement par défaut.
# /dev/sda1 /, /dev/sda2 /home.
# / doit être sur /dev/sda1. Sinon adapter le script !
# Enfin un fstab est généré, lvm2 est installé, grub et le kernel sont réinstallés.

export LC_ALL=C
swapoff -a
cd /mnt
mkdir root_in_ram rootfs home var usr log
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/sda1 /mnt/rootfs/
rsync -a /mnt/rootfs/ /mnt/root_in_ram/
umount /mnt/rootfs

# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt

# bios_grub GPT *mandatory*
parted -a minimal -s /dev/sda mkpart primary 0M 1M
parted -s /dev/sda set 1 bios_grub on

# /boot
parted -a minimal -s /dev/sda mkpart primary ext4 1M 200M
parted -s /dev/sda set 2 boot on
mkfs.ext4 -LBOOT /dev/sda2 >/dev/null

# /
parted -a optimal -s /dev/sda mkpart primary ext4 200M 1200M
mkfs.ext4 -LROOTFS /dev/sda3 >/dev/null

# /var
parted -a optimal -s /dev/sda mkpart primary ext4 1200M 11200M
mkfs.ext4 -LVAR /dev/sda4 >/dev/null

# /usr
parted -a optimal -s /dev/sda mkpart primary ext4 11200M 16200M
mkfs.ext4 -LUSR /dev/sda5 >/dev/null

# LVM
parted -a minimal -s /dev/sda mkpart primary ext4 16200M 100%
parted -s /dev/sda set 6 lvm on


# Add LVM with a VG for all of the free space.
pvcreate /dev/sda6
vgcreate SSD0 /dev/sda6
pvcreate /dev/sdb
vgcreate HDD0 /dev/sdb
lvcreate --size 1G --name tmp SSD0
mkfs.ext4 -LTMP /dev/mapper/SSD0-tmp >/dev/null
lvcreate --size 512M --name swap1 SSD0
lvcreate --size 512M --name swap2 HDD0
mkswap -f -LSWAP1 /dev/mapper/SSD0-swap1
mkswap -f -LSWAP2 /dev/mapper/HDD0-swap2
lvcreate --size 200G --name home HDD0
mkfs.ext4 -LHOME /dev/mapper/HDD0-home >/dev/null
lvcreate --size 40G --name srv SSD0
mkfs.ext4 -LSRV /dev/mapper/SSD0-srv >/dev/null

# Copy data from RAM.
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mount -LUSR usr
rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/
umount home var usr

# Generate fstab.
cat <<EOT >rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SRV       /srv    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run
rm -rf /mnt/rootfs/boot/*
chroot /mnt/rootfs/ bash
export LC_ALL=C
mount /boot
mount /usr
mount /var
mount /tmp
chmod 1777 /tmp

apt purge -y grub-common grub-pc grub-pc-bin grub2-common

for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done

# LVM2 déjà OK sur machines OVH
#apt install -y lvm2
apt install -y grub-common grub-pc grub-pc-bin grub2-common linux-image-amd64
passwd
sed -i 's/without-password/yes/' /etc/ssh/sshd_config
exit

echo "Evolix partitioning done... You can now reboot!"

#!/bin/bash
 
# Le script suppose que vous avez installé votre serveur avec le partionnement par défaut.
# /dev/md2 / 20G, /dev/md3 /home
# / doit être sur /dev/md2. Sinon adapter le script !
# Enfin un fstab est généré, lvm2 est installé, grub et le kernel sont réinstallés.

export LC_ALL=C
swapoff -a
cd /mnt
mkdir root_in_ram rootfs home var usr log
mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md2 /mnt/rootfs/
rsync -a /mnt/rootfs/ /mnt/root_in_ram/
umount /mnt/rootfs
mdadm --stop /dev/md2
mdadm --stop /dev/md3
# Create a GPT label. (Removes all parts).
parted -s /dev/sda mklabel gpt
# bios_grub GPT *mandatory*
parted -a minimal -s /dev/sda mkpart primary 0M 1M
parted -s /dev/sda set 1 bios_grub on
# /boot
parted -a minimal -s /dev/sda mkpart primary ext4 1M 200M
parted -s /dev/sda set 2 boot on
parted -s /dev/sda set 2 raid on
# /
parted -a optimal -s /dev/sda mkpart primary ext4 200M 1200M
parted -s /dev/sda set 3 raid on
# /var
parted -a optimal -s /dev/sda mkpart primary ext4 1200M 11200M
parted -s /dev/sda set 4 raid on
# /usr
parted -a optimal -s /dev/sda mkpart primary ext4 11200M 16200M
parted -s /dev/sda set 5 raid on
# LVM
parted -a minimal -s /dev/sda mkpart primary ext4 16200M 100%
parted -s /dev/sda set 6 raid on

#Copy parts to sdb
sgdisk -R=/dev/sdb /dev/sda
sgdisk -G /dev/sdb
partprobe /dev/sda
partprobe /dev/sdb

# RAID1 for systems.
# Metadata 0.90 as some OVH kernel need it to boot.
for part in {2,3,4,5}; do
    mdadm --create /dev/md${part} --metadata=0.90 --level=raid1 --raid-devices=2 /dev/{sda,sdb}${part}
done
# No need for LVM part to has metadata 0.90.
yes | mdadm --create /dev/md6 --level=raid1 --raid-devices=2 /dev/{sda,sdb}6

mkfs.ext4 -LBOOT /dev/md2 >/dev/null
mkfs.ext4 -LROOTFS /dev/md3 >/dev/null
mkfs.ext4 -LVAR /dev/md4 >/dev/null
mkfs.ext4 -LUSR /dev/md5 >/dev/null

# Add LVM with a VG for all of the free space.
pvcreate /dev/md6
vgcreate vg0 /dev/md6
lvcreate --size 1G --name tmp vg0
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null
lvcreate --size 512M --name swap1 vg0
lvcreate --size 512M --name swap2 vg0
mkswap -f -LSWAP1 /dev/mapper/vg0-swap1
mkswap -f -LSWAP2 /dev/mapper/vg0-swap2
lvcreate --size 10G --name home vg0
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null
lvcreate --extents 95%FREE --name backup vg0
mkfs.btrfs -LBACKUP /dev/vg0/backup

# Copy data from RAM.
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mount -LUSR usr
rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/
umount home var usr

# Generate fstab.
cat <<EOT>rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=BACKUP    /backup btrfs   defaults 0 2
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run
rm -rf /mnt/rootfs/boot/*
chroot /mnt/rootfs/ bash
export LC_ALL=C
mount /boot
mount /usr
mount /var
mount /tmp
chmod 1777 /tmp
apt purge -y grub-common grub-pc grub-pc-bin grub2-common
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
/usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf
# LVM2 déjà OK sur machines OVH
#apt install -y lvm2
apt install -y grub-common grub-pc grub-pc-bin grub2-common linux-image-amd64
passwd
sed -i 's/without-password/yes/' /etc/ssh/sshd_config
exit

echo "Evolix partitioning done... You can now reboot!"

#!/bin/bash
 
# Le script suppose que vous avez installé votre serveur avec le partionnement par défaut.
# /dev/md3 /, /dev/md4 /home.
# / doit être sur /dev/md3. Sinon adapter le script !
# Enfin un fstab est généré, le raid est configuré, lvm2 est installé, grub et le kernel sont réinstallés.

export LC_ALL=C

swapoff -a

cd /mnt

mkdir root_in_ram rootfs home var usr log

mount -t tmpfs none /mnt/root_in_ram -o size=90%
mount /dev/md3 /mnt/rootfs/

rsync -a /mnt/rootfs/ /mnt/root_in_ram/

umount /mnt/rootfs

# Change LABEL for /boot, yeah 2 times, IDK why but the first time is not applied Oo
mdadm --stop /dev/md4
mdadm --stop /dev/md3

tune2fs -LBOOT /dev/md2 && tune2fs -LBOOT /dev/md2

parted -s /dev/sda rm 5
parted -s /dev/sda rm 4
parted -s /dev/sda rm 3
# /
parted -a optimal -s /dev/sda mkpart primary ext4 1073M 2073M
sleep 2
parted -s /dev/sda set 3 raid on
# /var
parted -a minimal -s /dev/sda mkpart primary ext4 2073M 12073M
sleep 2
parted -s /dev/sda set 4 raid on
# /usr
parted -a minimal -s /dev/sda mkpart primary ext4 12073M 17073M
sleep 2
parted -s /dev/sda set 5 raid on
# LVM
parted -a minimal -s /dev/sda mkpart primary ext4 17073M 100%
parted -s /dev/sda set 6 raid on
sleep 2

parted -s /dev/sdb rm 5
parted -s /dev/sdb rm 4
parted -s /dev/sdb rm 3
# /
parted -a optimal -s /dev/sdb mkpart primary ext4 1073M 2073M
sleep 2
parted -s /dev/sdb set 3 raid on
# /var
parted -a minimal -s /dev/sdb mkpart primary ext4 2073M 12073M
sleep 2
parted -s /dev/sdb set 4 raid on
# /usr
parted -a minimal -s /dev/sdb mkpart primary ext4 12073M 17073M
sleep 2
parted -s /dev/sdb set 5 raid on
# LVM
parted -a minimal -s /dev/sdb mkpart primary ext4 17073M 100%
parted -s /dev/sdb set 6 raid on
sleep 2

# RAID1 for systems.
# Metadata 0.90 as some OVH kernel need it to boot.
for part in {3,4,5}; do
    mdadm --create /dev/md${part} --metadata=0.90 --level=raid1 --raid-devices=2 /dev/{sda,sdb}${part}
done
# No need for LVM part to has metadata 0.90.
yes | mdadm --create /dev/md6 --level=raid1 --raid-devices=2 /dev/{sda,sdb}6

mkfs.ext4 -LROOTFS /dev/md3 >/dev/null
mkfs.ext4 -LVAR /dev/md4 >/dev/null
mkfs.ext4 -LUSR /dev/md5 >/dev/null

# Add LVM with a VG for all of the free space.
pvcreate /dev/md6
vgcreate vg0 /dev/md6

lvcreate --size 1G --name tmp vg0
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null

lvcreate --size 512M --name swap1 vg0
lvcreate --size 512M --name swap2 vg0
mkswap -f -LSWAP1 /dev/mapper/vg0-swap1
mkswap -f -LSWAP2 /dev/mapper/vg0-swap2

lvcreate --size 20G --name log vg0
mkfs.ext4 -LLOG /dev/mapper/vg0-log >/dev/null

lvcreate --extents 95%FREE --name home vg0
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null

# Copy data from RAM.
mount -LROOTFS rootfs
mount -LHOME home
mount -LVAR var
mkdir var/log
mount -LLOG var/log
mount -LUSR usr

rsync -a root_in_ram/home/ home/
rsync -a root_in_ram/var/ var/
rsync -a root_in_ram/usr/ usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/

umount home var/log var usr

# Generate fstab.
cat <<EOT>rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=EFI_SYSPART /boot/efi   vfat   defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=LOG       /var/log    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 0
LABEL=SWAP2     none    swap    sw 0 0
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*

chroot /mnt/rootfs/ bash

export LC_ALL=C

mount /boot
mount /dev/sda1 /boot/efi
mount /usr
mount /var
mount /tmp

chmod 1777 /tmp

apt purge -y grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done

/usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf

apt install -y lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64 dosfstools

update-grub2

passwd
sed -i 's/without-password/yes/' /etc/ssh/sshd_config
exit

echo "Evolix partitioning done... You can now reboot!"

#!/bin/bash

echo "ce script ne doit pas être joué automatiquement"
echo "les commandes sont à jouer manuellement, par copier/coller"
exit 1

# Le script suppose que vous avez installé votre dedibox avec le partionnement par défaut.
# UEFI p1 fait 6OOM, /boot p2 fait 600M, SWAP p3, / p4.
# / doit être sur md127. Sinon adapter le script !
# Enfin un fstab est généré, grub et le kernel sont réinstallés.

ROOT_PART=/dev/md127
export LC_ALL=C
# disable swap
swapoff -a
cd /mnt

# mount /
mkdir root_in_ram rootfs home var usr log
mount $ROOT_PART /mnt/rootfs/

# copy all the filesystem in ram
mount -t tmpfs none /mnt/root_in_ram -o size=90%
rsync -a /mnt/rootfs/ /mnt/root_in_ram/

# unount rootfs to recreate partitions
umount /mnt/rootfs

# stop soft raid
mdadm --stop /dev/md{125..127}
# Wipe all signatures
for i in /dev/nvme?n?p?; do wipefs -a $i; done

# NOTE: sleep 1 second between each command
#       to be able to copy/paste the whole bloc

# Create a GPT label. (Removes all parts).
parted -s /dev/nvme0n1 mklabel gpt
sleep 1
# boot,esp GPT/UEFI *mandatory*
parted -a minimal -s /dev/nvme0n1 mkpart primary 0M 100M
sleep 1
parted -s /dev/nvme0n1 set 1 boot on
parted -s /dev/nvme0n1 set 1 esp on
sleep 1
# /boot
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 100M 600M
sleep 1
parted -s /dev/nvme0n1 set 2 raid on
sleep 1
# /
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 600M 1100M
sleep 1
parted -s /dev/nvme0n1 set 3 raid on
sleep 1
# /usr
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 1100M 7100M
sleep 1
parted -s /dev/nvme0n1 set 4 raid on
sleep 1
# swap1
parted -a minimal -s /dev/nvme0n1 mkpart primary linux-swap 7100M 17100M
sleep 1
# LVM
parted -a minimal -s /dev/nvme0n1 mkpart primary ext4 17100M 100%
sleep 1
parted -s /dev/nvme0n1 set 6 raid on
sleep 1

#Copy parts to nvme1n1
for i in /dev/nvme[123]n1; do
    sgdisk -R=${i} /dev/nvme0n1
    sgdisk -G $i
    partprobe $i
done

# RAID10 for systems.
for part in {2,3,4,6}; do
    mdadm --create /dev/md${part} --metadata=1.2 --level=raid10 --raid-devices=4 \
      /dev/{nvme0n1p${part},nvme1n1p${part},nvme2n1p${part},nvme3n1p${part}}
done

# Make filesystems
l=1
for i in /dev/nvme?n1p[5]; do
    mkswap -f -LSWAP${l} $i 1>/dev/null
    l=$((l+1))
done

apt install dosfstools
# format the boot-efi partition on all disks
for i in /dev/nvme?n1p[1]; do
    mkfs.vfat -F32 $i >/dev/null
done
# Add label on first disk only
fatlabel /dev/nvme0n1p1 EFI_SYSPART

mkfs.ext4 -LROOTFS /dev/md3 >/dev/null
sleep 1
mkfs.ext4 -LBOOT /dev/md2 >/dev/null
sleep 1
mkfs.ext4 -LUSR /dev/md4 >/dev/null
sleep 1

l=1
for i in /dev/nvme?n1p[5]; do
    mkswap -f -LSWAP${l} $i 1>/dev/null
    l=$((l+1))
done

# Add LVM with a VG for all of the free space.
pvcreate /dev/md6
sleep 1
vgcreate vg0 /dev/md6
sleep 1

lvcreate --size 1G --name tmp vg0
sleep 1
mkfs.ext4 -LTMP /dev/mapper/vg0-tmp >/dev/null
sleep 1

lvcreate --size 5G --name var vg0
sleep 1
mkfs.ext4 -LVAR /dev/mapper/vg0-var >/dev/null
sleep 1

### Pour un serveur normal
# lvcreate -l 98%FREE --name home vg0
# sleep 1
# mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null
# sleep 1
### Pour un KVM
lvcreate --size 10G --name home vg0
sleep 1
mkfs.ext4 -LHOME /dev/mapper/vg0-home >/dev/null
sleep 1
lvcreate -l 98%FREE --name srv vg0
sleep 1
mkfs.ext4 -LSRV /dev/mapper/vg0-srv >/dev/null
sleep 1
###############

# Copy data from RAM.
mount -LROOTFS rootfs
mkdir -p rootfs/home && mount -LHOME rootfs/home
mkdir -p rootfs/var && mount -LVAR rootfs/var
mkdir -p rootfs/usr && mount -LUSR rootfs/usr
sleep 1

rsync -a root_in_ram/home/ rootfs/home/
rsync -a root_in_ram/var/ rootfs/var/
rsync -a root_in_ram/usr/ rootfs/usr/
rsync -a \
    --exclude="home/**" \
    --exclude="var/**" \
    --exclude="usr/**" \
    root_in_ram/ rootfs/
sleep 1

# Generate fstab.
cat <<EOT>rootfs/etc/fstab
LABEL=ROOTFS    /       ext4    errors=remount-ro 0 1
LABEL=BOOT      /boot   ext4    defaults 0 2
LABEL=EFI_SYSPART /boot/efi   vfat   defaults 0 2
LABEL=HOME      /home   ext4    defaults 0 2
LABEL=TMP       /tmp    ext4    defaults 0 2
LABEL=USR       /usr    ext4    defaults 0 2
LABEL=VAR       /var    ext4    defaults 0 2
LABEL=SWAP1     none    swap    sw 0 2
LABEL=SWAP2     none    swap    sw 0 2
LABEL=SWAP3     none    swap    sw 0 2
LABEL=SWAP4     none    swap    sw 0 2
EOT

# Chroot and install lvm2 + reconfigure grub-pc
mount -t proc none /mnt/rootfs/proc
mount -o bind /dev /mnt/rootfs/dev
mount -o bind /dev/pts /mnt/rootfs/dev/pts
mount -t sysfs sys /mnt/rootfs/sys
mount --bind /run /mnt/rootfs/run

rm -rf /mnt/rootfs/boot/*
sleep 1

chroot /mnt/rootfs/ bash
sleep 1

export LC_ALL=C

mkdir -p /boot && mount /boot
mkdir -p /boot/efi && mount /dev/nvme0n1p1 /boot/efi
mount /tmp
chmod 1777 /tmp
sleep 1


# apt install -y debconf-utils apt-utils
# sleep 1
# cat <<EOT>/tmp/grub-selections
# grub-efi-amd64    grub2/force_efi_extra_removable    boolean    true
# grub-efi-amd64    grub2/linux_cmdline_default    string    quiet
# grub-efi-amd64    grub2/update_nvram    boolean    true
# grub-efi-amd64    grub2/linux_cmdline    string
# grub-pc    grub-pc/postrm_purge_boot_grub    boolean    true
# EOT
# sleep 1
# debconf-set-selections /tmp/grub-selections
# sleep 1
# dpkg-reconfigure --frontend=noninteractive grub-pc
# sleep 1

# purge des paquets de Grub et du noyau, pour réinitialiser complètement les config
rm -rf /etc/default/grub.d
apt purge -y os-prober grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub-pc grub2-common
sleep 1

# purge des kernels
for kernel_pkg in $(dpkg -l | grep linux-image | awk '{ print $2 }'); do apt purge -y $kernel_pkg; done
sleep 1
# réinstallation des paquets
apt install -y lvm2 grub-common grub-efi grub-efi-amd64 grub-efi-amd64-bin grub2-common linux-image-amd64
sleep 1
apt purge -y os-prober

# En cas d'erreur à l'install/config de Grub, il faut vérifier si le RAID est OK
# cat/proc/mdstat

grub-install /dev/nvme0n1
sleep 1
grub-install /dev/nvme1n1
sleep 1
grub-install /dev/nvme2n1
sleep 1
grub-install /dev/nvme3n1
sleep 1

# Questions "update-grub":
# * Linux command line:
# * Linux default command line: quiet
# * Force extra installation to the EFI removable media path? Yes
# * Update NVRAM variables to automatically boot into Debian? Yes
update-grub
# dpkg-reconfigure --frontend=noninteractive grub-efi-amd64
dpkg-reconfigure grub-efi-amd64

sleep 1

# paquets utiles pour evolixisation par Ansible
apt install -y sudo python python-apt
sleep 1

exit

echo "Evolix partitioning done... You can now reboot!"
